const fs = require('fs');

// creating new directory

function createDirectory(directoryName, callbackFunction) {

    const directoryPath = `${__dirname}/${directoryName}`;

    fs.mkdir(directoryPath, (err) => {
        if (err) {
            callbackFunction(err);
        } else {
            callbackFunction(err);
        }
    });
}

// creating random number of JSON files

function createFile(directoryName, numberOfFiles, callbackFunction) {

    let countFile = 1;
    let countSuccess = 0;
    let countFail = 0;

    function recursiveCreateFile(countFile) {

        if (countFile > numberOfFiles) {
            return;
        }

        const filePath = `${__dirname}/${directoryName}/file${countFile}.json`;

        const data = {
            id: countFile,
            name: `name${countFile}`,
        };

        fs.writeFile(filePath, JSON.stringify(data), (err) => {
            if (err) {
                countFail += 1;
            } else {

                countSuccess += 1;
                console.log("File created");
            }

            if (countFail + countSuccess === numberOfFiles) {
                callbackFunction(err);
            }
        });

        recursiveCreateFile(countFile + 1);
    }
    recursiveCreateFile(countFile);
}

// deleting random number of JSON files

function deleteFile(directoryName, numberOfFiles, callbackFunction) {

    let countFile = 1;
    let countSuccess = 0;
    let countFail = 0;

    function recursiveCreateFile(countFile) {

        if (countFile > numberOfFiles) {
            return;
        }

        const filePath = `${__dirname}/${directoryName}/file${countFile}.json`;

        fs.unlink(filePath, (err) => {
            if (err) {
                countFail += 1;
            } else {

                countSuccess += 1;
                console.log("File deleted");
            }

            if (countFail + countSuccess === numberOfFiles) {
                callbackFunction(err);
            }
        });

        recursiveCreateFile(countFile + 1);
    }
    recursiveCreateFile(countFile);
}

module.exports = { createDirectory, createFile, deleteFile };